#!usr/bin/perl
# This script is a part of gFACs: https://gitlab.com/PlantGenomicsLab/gFACs
# Copyright (C) 2018 Madison Caballero
###################################################################################################################################################################
# SOLVING THE MOST IDIOTIC PROBLEM 
#	About this script:
#		This script is designed to filter out any stop codons that are not in frame. I look for stop codons without taking into account frame, so if the
#	last three nucleotides are a stop codon, you would think it would be a clear termination. Right? Wrong. Because sometimes the gene runs into the end
#	or beginning (if -) of a scaffold and those last three nucelotides in the scaffold are a stop codon. Very rare but it DOES happen. This just ensures that
#	genes are divisable by 3 if they pass the stop codon filter!
###################################################################################################################################################################

$gene_table = "$ARGV[1]" . "\/" . "\/" . $ARGV[2] . "gene_table.txt";							# Calling the gene table

##ARGV[1] --> Location
##ARGV[2] --> Pre

open (GENE_TABLE, "$gene_table") or die "Cannot open the gene_table.txt file at $ARGV[1]";				# Gene table process.
        @gene_table = <GENE_TABLE>;
        $gene_table = join "", @gene_table;
        @gene_table = split "###", $gene_table;
close GENE_TABLE;

$output = $ARGV[1] . "\/" . "\/" . $ARGV[2] . "frame_checked_gene_table.txt";						# Name the output
open (OUTFILE, ">$output");												# Create the output
	print OUTFILE "###", "\n";	
###################################################################################################################################################################
GENE:foreach $gene (@gene_table){											# We're parsing!
        if ($gene =~ /gene/){												# Are we looking at a gene?
		$frame = 0;												# Paranoia number reset early	
		$exon_size = 0;
                $number_of_genes++;
        	$gene =~ s/^\s+\n//g;
                @split_lines = split "\n", $gene;
 
                       foreach $line (@split_lines){									# Parsing the gene
                                $line =~ s/\s+$//;
                                @tab = split "\t", $line;
                                if ($tab[0] =~ /exon/){$exon_size = $exon_size + $tab[1];}				# We're tallying the CDS
                        }
			$frame = $exon_size / 3;									# Divide CDS by 3
			if ($frame =~ /\./){next GENE;}									# Decimal. Clearly not divisible by 3
			if ($frame !~ /\./){										# Non decimal = good job being a complete gene
				print OUTFILE $gene, "\n", "###";
				$saved++;
			}
	}
}
close OUTFILE;

$lost = $number_of_genes - $saved;											# Results 
print "Number of genes retained:\t", $saved, "\n";
print "Number of genes lost:\t", $lost, "\n";

###################################################################################################################################################################
###################################################################################################################################################################
# Did you know the original US supreme only had six members? 
