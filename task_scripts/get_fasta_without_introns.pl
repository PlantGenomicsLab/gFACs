#!usr/bin/perl
# This script is a part of gFACs: https://gitlab.com/PlantGenomicsLab/gFACs
# Copyright (C) 2018 Madison Caballero
##################################################################################################################################################################
# THE GREAT FAA
#	About this script:
#		We are essentially doing the same thing as creating the nucleotide fasta, void of intron sequences, but we are going to translate it! Steps have
#	been taken before to ensure things are in frame and that exons are in the correct order. Translations are far more unforgiving as errors will be 
#	more notable if you look at the translation. If you specify the need for a start and stop codon, all genes should start with M and end with *!
##################################################################################################################################################################

# ARGV[0] --> in fasta
# ARGV[1] --> output location
# ARGV[2] --> prefix

use Bio::Index::Fasta;											# Using our good friend bioperl

$output = "$ARGV[1]" . "\/" . "\/" . $ARGV[2] . "genes.fasta";			# Name the output
open (FASTA_1, ">$output") or die "Cannot create the output script!!!";					# Create the output

$input = $ARGV[0] . ".idx";										# Name the index
$inx = Bio::Index::Fasta->new(-filename => $input);							# Get the index

$gene_table = "$ARGV[1]" . "\/" . "\/" . $ARGV[2] . "gene_table.txt";					# Gene table and storage of information
open (GENE_TABLE, "$gene_table") or die "Cannot open $gene_table";
	@gene_table = <GENE_TABLE>;
	$gene_table = join "", @gene_table;
	@gene_table = split "###", $gene_table;
close GENE_TABLE;

##################################################################################################################################################################
$old_seq = "new";
GENE: foreach $gene (@gene_table){									# Parsing genes
        $gene =~ s/\s+$//g;
        @split_lines = split "\n", $gene;
        foreach $line (@split_lines){
                $line =~ s/\s+$//;
	$line =~ s/\;COMP//;
	$line =~ s/\;5_INC\+3_INC//;
        $line =~ s/\;3_INC//;
        $line =~ s/\;5_INC//;
                @split_tab = split "\t", $line;
                        if ($split_tab[0] =~ /gene/){							# Gene line provides the header
                                print FASTA_1 "\>", $split_tab[5], "\n";
					if ($split_tab[6] ne $old_seq){
                                                $seq = $inx->fetch("$split_tab[6]");                            # Gets scaffold sequence
                                        }
                                        $old_seq = $split_tab[6];
                  	}
			if ($split_tab[0] =~ /exon/){							# Exon sequences are all we care about
                                $start = $split_tab[2];
                                $end = $split_tab[3];

                                $string_1 = $seq->subseq("$start", "$end");				
				push @sequence, $string_1;						# Store the sequence
			}
	}                         
      		$join_string = join "", @sequence;							# Join together all the exon sequence
		@string = split "", $join_string;							# Then break it up to nt by nt

	if ($split_tab[4] =~ /\+/){
  	  for ($a = 0; $a < scalar @string; $a++){							# Parsing nt
		$nt = $string[$a];								
		$nt =~ tr/atgc/ATGC/;
                push @nt_sequence, $nt; 	                                                # Positive strand doesn't need anything fancy
	  }
	}
	if ($split_tab[4] =~ /\-/){
		for ($a = scalar @string - 1; $a >= 0; $a--){					# If negative, we are switching and inverting the sequences
			$nt = $string[$a];
                       	$nt =~ tr/atgc/ATGC/;
			$nt =~ tr/ATGC/TACG/;
			push @nt_sequence, $nt;							# Then storing it in reverse for a forward M-->* protein
		}
	}
	if (scalar @nt_sequence > 0){
		print FASTA_1 @nt_sequence, "\n";
	}									# Print the translation
        undef (@sequence);
	undef (@nt_sequence); 
}
close FASTA_1;
##################################################################################################################################################################
##################################################################################################################################################################
# Did you know Celsius and Fahrenheit are equal at -40 degrees.

