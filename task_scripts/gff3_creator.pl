#!usr/bin/perl
# This script is a part of gFACs: https://gitlab.com/PlantGenomicsLab/gFACs
# Copyright (C) 2018 Madison Caballero
##################################################################################################################################################################
# THE GREAT CONVERSION
#	About this script:
#		This script does 2 things. 1, it goes back and takes the scores from the original input file. And 2, it formats the gene table into the gff3v3
#	output. The gff3 format comes from https://useast.ensembl.org/info/website/upload/gff3.html.
##################################################################################################################################################################
#$ARGV[0] = start stop gene table
#$ARGV[1] = output location
#$ARGV[3] = prefix
#$ARGV[2] = input gene file.
#$ARGV[4] = simple/null

open (INPUT, $ARGV[2]) or die "Cannot open $ARGV[2]";			# We are getting the ORIGINAL input.

while ($line = <INPUT>){						# Parse the input
	$line =~ s/\s+$//;
	undef (@array);
        undef (@sort);
	@split_line = split "\t", $line;
		push @array, $split_line[3];   				# start
		push @array, $split_line[4];   				# stop
		
	@sort = sort {$a <=> $b} @array;				# correcting for potential disorder
		$ID = $sort[0] . "_" . $sort[1];			# Making ID
	
	if ($Score_to_ID{$ID} =~ /\d/){					# If a number is already stored
		if ($Score_to_ID{$ID} < $split_line[5]){		# And if the number stored is smaller
			$Score_to_ID{$ID} = $split_line[5];		# Replace it
		}
	}
	else{
		$Score_to_ID{$ID} = $split_line[5];			# Otherwise store what you've got
	}
	undef (@array);
	undef (@sort);
}
close INPUT;
##################################################################################################################################################################
$output = $ARGV[1] . "\/" . "\/" . $ARGV[3] . "out\.gff3";		# Name the output

open (GFF3, ">$output") or die "Cannot create $output";			# Creat the output

open (GENE_TABLE, $ARGV[0]) or die "Cannot open $ARGV[0]";		# Our friend the gene table

print GFF3 "\#\#gff-version 3", "\n";					# Required version line

while ($line = <GENE_TABLE>){
	$line =~ s/\s+$//;
	$line =~ s/\;COMP//;
        $line =~ s/\;5_INC\+3_INC//;
        $line =~ s/\;3_INC//;
        $line =~ s/\;5_INC//;

	if ($line =~ /\#\#\#/){next;}					# We don't care about partition lines
	if ($line =~ /./){
		@split_line = split "\t", $line;
		$start_stop = $split_line[2] . "_" . $split_line[3];
		$score = ".";
		if ($Score_to_ID{$start_stop} =~ /./){
			$score = $Score_to_ID{$start_stop};		# Score reset
		}
		$split_line[0] =~ s/gene/mRNA/;				# Formatting ...
		if ($split_line[0] =~ /mRNA/){ 
			$intron = 0;
			$exon = 0;
			$split_line[5] =~ s/ID\=//;
			$parent =  $split_line[5];
			$part = "ID=" . $split_line[5] . 
				"\;" . "Name=" . $split_line[5] . "\;";
		}

		if ($split_line[0] =~ /exon/){
			$exon++;
                        $part = "ID=" . $parent . "\.exon" . $exon .
                                "\;" . "Parent=" . $parent . "\;";
                }

		 if ($split_line[0] =~ /intron/){
                        $intron++;
                        $part = "ID=" . $parent . "\.intron" . $intron .
                                "\;" . "Parent=" . $parent . "\;";
                }
	
									# Printing to outfile:
		print GFF3 $split_line[6], "\t",			# scaffold
			"GFACS", "\t",					# Source. Me!
			$split_line[0], "\t",					# Gene part
			$split_line[2], "\t",				# Start
			$split_line[3], "\t",				# Stop
			$score, "\t",					# Score, if we have it. "." if not.
			$split_line[4], "\t",				# Strand
			".", "\t",					# Something I cannot provide
			$part, "\n";				# Information
	}
}		
close GENE_TABLE;
close GFF3;
##################################################################################################################################################################
##################################################################################################################################################################
# It is illegal to die in Longyearbyen, Norway. 
