#!usr/bin/perl
# This script is a part of gFACs: https://gitlab.com/PlantGenomicsLab/gFACs
# Copyright (C) 2018 Madison Caballero
##########################################################################################################################################################
# CHECKING TO ENSURE SCAFFOLDS MATCH
##########################################################################################################################################################
# Arguments:
# ARGV[0] --> in fasta
# ARGV[1] --> output location
##########################################################################################################################################################
$input = $ARGV[0] ;
$output_file =  "$ARGV[1]" . "\/" . "\/" . $ARGV[2] . "scaffolds.txt"; 
system("grep \"\>\" $input \> $output_file");
open(SCAFFOLDS, $output_file);
while($line = <SCAFFOLDS>){
	$line =~ s/\s+$//;
	$line =~ s/^\>//;	
	$scaffolds{$line}++;
}
close SCAFFOLDS;
system("rm $output_file");

##########################################################################################################################################################
$gene_table = "$ARGV[1]" . "\/" . "\/" . $ARGV[2] . "gene_table.txt";							# Gene table info
open (GENE_TABLE, "$gene_table") or die "Cannot open the gene_table.txt file at $ARGV[1]";
	@gene_table = <GENE_TABLE>;
	$gene_table = join "", @gene_table;
	@gene_table = split "###", $gene_table;
close GENE_TABLE;
##########################################################################################################################################################
$old_seq = "new";
GENE: foreach $gene (@gene_table){
	$start_check = 0;
	if ($gene =~ /gene/){$number_of_genes++;}
	$gene =~ s/^\s+\n//g;
        @split_lines = split "\n", $gene;
        foreach $line (@split_lines){
		$line =~ s/\s+$//;
                @split_tab = split "\t", $line;
                        if ($split_tab[0] =~ /gene/){
				if($scaffolds{$split_tab[6]} !~ /./){	$no_match++; $nonmatch{$split_tab[6]}++; }
			}
	}
}
close OUT_TABLE;

$kill = "$ARGV[1]" . "\/" . "\/" . $ARGV[2] . "kill_command.txt";
open(KILL, ">$kill"); print KILL "0"; close KILL;

if ($no_match > 0){
	print "ERROR: Chromosomes in anotation do not match the genome header. ",
	 scalar keys %nonmatch, " scaffolds found that do not match the index.", "\n";
	 $kill = "$ARGV[1]" . "\/" . "\/" . $ARGV[2] . "kill_command.txt";
	 open(KILL, ">$kill"); print KILL "1"; close KILL;
} 

##########################################################################################################################################################
# Ginkgo trees have survived nuclear bombs in locations where all other plants and animals did not.


