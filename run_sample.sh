# Sample command for gFACs
# Note the indentations do not matter, they just help visualize the steps
# Not EVERY flag is included here 

gFACs.pl \
-f braker_2.05_gff \
-p FULL_TEST \
        --statistics \
        --statistics-at-every-step \
        --rem-5prime-3prime-incompletes \
        --rem-all-incompletes \
        --min-exon-size 100 \
        --min-intron-size 200 \
        --min-CDS-size 100 \
        --unique-genes-only \
--fasta hg19.fa \
		--splice-table \
              	--canonical-only \
                --rem-genes-without-start-codon \
                --allow-alternate-starts \
                --rem-genes-without-stop-codon \
                --rem-genes-without-start-and-stop-codon \
                --allowed-inframe-stop-codons 0 \
                --nt-content \
                --get-fasta-with-introns \
                --get-fasta-without-introns \
                --get-protein-fasta \
                --create-gtf \
                --create-simple-gtf \
                --create-gff3 \
--entap-annotation /my/entap/outfiles/final_annotations_lvl4_no_contam.tsv \
                --annotated-all-genes-only \
--distributions \
                exon_lengths 10 \
                intron_lengths 15 \
                CDS_lengths 20 \
                gene_lengths 100 \
                exon_position \
                exon_position_data \
                intron_position \
                intron_position_data \
 --compatibility \
                SnpEff \
                EVM_1.1.1_gene_prediction \
                EVM_1.1.1_alignment \
-O . \
augustus.hints.gff
