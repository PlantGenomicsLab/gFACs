Installation
================================= 

Installing gFACs is quite easy since this program operates almost exclusively through perl scripts.  So long as the files are copied over, basic gFACs should be operational so long as `perl <https://www.perl.org/>`_ and `bioperl <https://bioperl.org/>`_ libraries are available. gFACs is also very light so feel free to have multiple copies wherever you need it! 

You can find gFACs on `GitLab <https://gitlab.com/PlantGenomicsLab/gFACs>`_.


**In order to download gFACs, follow these basic steps:**

1. Obtain the directory for gFACs from GitLab in whatever format you prefer:

.. image:: Download_1.JPG
    :align: center
    :scale: 75%
    
    
2. Place the entire folder onto your system and extract components. You should have a screen that looks like this:

.. image:: Download_2.JPG
    :align: center
    :scale: 100%
    

3. To test to see if it works, perform the simple command of: 

    ``perl gFACs.pl``

The command line manual should appear. If so, congratulations! If not, a common error that occurs is that bioperl libraries are not loaded. If you have issues, feel free
to send an issue request on GitLab!
    


