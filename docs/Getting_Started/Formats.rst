Supported Input Formats
============================

There are many different programs that utilize a gff or gtf style format. Each has its own inclusion rules and formatting guidelines. Therefore, a true universal gff/gff3/gtf file converter that requires no user input may be an impossible task. gFACs requires the user to define the application source and version. If you do not find you input format, look into the support script format_diagnosis.pl. If your file is made up of many different formats merged together, I suggest breaking it apart. If you want a format added (especially if it is a well-known one) let me know and I will likely create one!

The format is specified in the gFACs command by a **-f [code]** flag. It is a **mandatory** flag and the code will fail without it. These codes are listed out below with notes and can also be seen in the command line manual. For an example of the command with a proper format flag, see any of the sample runs!


.. |br| raw:: html

    <br>

`BRAKER <https://github.com/Gaius-Augustus/BRAKER>`_\: |br|

 Braker format augustus.gff/gff3/gft:|br|
    braker_2.05_gtf |br|
    braker_2.05_gff |br|
    braker_2.05_gff3 |br|
    braker_2.0_gff3 |br|
    braker_2.0_gff |br|
    braker_2.0_gtf |br|
    braker_2.1.2_gtf  - Works for 2.1.0-2.1.5 |br|

 Braker format braker.gtf:|br|
    braker_2.1.5_gtf


`MAKER <http://yandell-lab.org/software/maker.html>`_:|br|
maker_2.31.9_gff 

    Maker may provide other information such as blastx 
    and protein2genome information. Currently, only maker 
    models of genes and exons will be considered.

`PROKKA <https://github.com/tseemann/prokka>`_: |br|
prokka_1.11_gff |br|

`GMAP <http://research-pub.gene.com/gmap/>`_: |br|
gmap_2017_03_17_gff3 |br|

`GENOMETHREADER <http://genomethreader.org/>`_: |br|
genomethreader_1.6.6_gff3 |br|

`STRINGTIE <https://ccb.jhu.edu/software/stringtie/>`_: |br|
stringtie_1.3.4_gtf |br|

`GFFREAD <http://ccb.jhu.edu/software/stringtie/gff.shtml>`_: |br|
gffread_0.9.12_gff3 |br|

`EXONERATE <https://www.ebi.ac.uk/about/vertebrate-genomics/software/exonerate>`_: |br|
exonerate_2.4.0_gff |br|

`EVIDENCE MODELER <https://evidencemodeler.github.io/>`_: |br|
EVM_1.1.1_gff3 |br|

`CoGe <https://genomevolution.org/coge/>`_: |br|
CoGe_1.0_gff |br|

`GFACS <https://gitlab.com/PlantGenomicsLab/gFACs>`_: |br|
gFACs_gene_table |br|
gFACs_gtf 

    You can input a gene table from gFACs, any version. However, 
    the prefix on the input will NOT be retained.

NCBI: |br|
`refseq_gff <https://www.ncbi.nlm.nih.gov/genbank/genomes_gff/>`_ - only CDS taken.
    

For those who are curious, each format has a special conversion script that transitions the input into the gene table. These are the scripts found in the format_scripts folder that comes along with gFACs. If you are feeling adventurous, you can make your own conversion script that creates the gene table and simply run gFACs with the gene table format code.

.. image:: Format.JPG
    :align: center

