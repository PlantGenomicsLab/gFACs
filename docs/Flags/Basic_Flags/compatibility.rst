Compatibility flags
===========================

Just as the gff3/gff/gtf file formats follow their owns rules, so do other software in needing specific input formats. Although the gFACs `gtf <https://gfacs.readthedocs.io/en/latest/Flags/Fasta%20Required%20Flags/output.html#create-gtf>`_ is fairly standard, a few modifications must be made before it can safely be used within other programs. This includes transitions to other formats such as gff or gff3!

Several formats are alrady compatible by default. --create-gtf output is compatible with Jbrowse and protein/nucleotide FASTAs are compatible with EnTAP!

These options are still being developed and user input is more than welcome! Do you not see a format you would like added? Let me know! 

To specify the compatibility arguments, use this flag:

*-*-compatibility [option] [option] etc... 
-------------------------------------------

Allows the creation of software compatible files. Available format options are:


SnpEff 
-------

A gff file called snpeff_format.gff will be created that can be used for `SnpEff <http://snpeff.sourceforge.net/>`_ build. This format can be used in the default. 


EVM_1.1.1_gene_prediction
-------
A gff file called EVM_1.1.1_gene_prediction_format.gff will be created that can be used as a gene prediction format for `EVidence Modeler <https://evidencemodeler.github.io/#Preparing_inputs>`_.

EVM_1.1.1_alignment
-------
A gff file called EVM_1.1.1_alignment_format.gff will be created that can be used as an alignment format for `EVidence Modeler <https://evidencemodeler.github.io/#Preparing_inputs>`_.

stringtie_1.3.6_gtf
--------
A gtf file called stringtie_1.3.6_format.gtf will be created that can be used as an input  for `StringTie <https://ccb.jhu.edu/software/stringtie/>`_.




