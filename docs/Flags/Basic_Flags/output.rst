Basic output flags
===========================
These are output flags that do not require the input of any fasta or EnTAP files. 

.. |br| raw:: html

    <br>

*-*-statistics
---------------------

Statistics will be run on the gene table and printed to statistics.txt. This command is performed by task_scripts/classic_stats.pl. If a prefix is used, the statistics file will be named accordingly. 

These are all the potential statistics in the reported format:

Number of genes: |br|
Number of monoexonic genes: |br|    
Number of multiexonic genes: |br|    
|br|
Number of positive strand genes: |br|       
Monoexonic: |br|    
Multiexonic: |br|    
|br|
Number of negative strand genes: |br|        
Monoexonic: |br|    
Multiexonic: |br|    
|br|
Average overall gene size: |br|     		
Median overall gene size: |br|      
Average overall CDS size: |br|      
Median overall CDS size:    |br|    
Average overall exon size:      |br|
Median overall exon size:       |br|
|br|
Average size of monoexonic genes:       |br|
Median size of monoexonic genes:        |br|
Largest monoexonic gene:        |br|
Smallest monoexonic gene:      |br|
|br|
Average size of multiexonic genes:  |br|    
Median size of multiexonic genes:   |br|    
Largest multiexonic gene:       |br|
Smallest multiexonic gene:     |br| 
|br|
Average size of multiexonic CDS:   |br|     
Median size of multiexonic CDS: |br|
Largest multiexonic CDS:      |br| 
Smallest multiexonic CDS:     |br|  
|br|
Average size of multiexonic exons:  |br|    
Median size of multiexonic exons:   |br|   
Average size of multiexonic introns:  |br|  
Median size of multiexonic introns:    |br| 
|br|
Average number of exons per multiexonic gene:  |br| 
Median number of exons per multiexonic gene:   |br| 
Largest multiexonic exon:      |br|
Smallest multiexonic exon:      |br|
Most exons in one gene: |br|
|br|
Average number of introns per multiexonic gene: |br|
Median number of introns per multiexonic gene:  |br|
Largest intron: |br|
Smallest intron: |br|

The following columns do not involve codons: |br|
Number of complete models:    |br|
Number of 5' only incomplete models:    |br|
Number of 3' only incomplete models:    |br|
Number of 5' and 3' incomplete models:  |br|


If your set is only monoexonics, a smaller version of the statistics will be printed that only contain the categories where monoexonic genes are evaluated. 

*-*-statistics-at-every-step
----------------------------
A statistical analysis of the gene table is run following every filtering step. This information is in the same format as regular *-*-statistics but prints to the **log** following the information line for each flag. To ensure statistics.txt is created at the end, make sure to include *-*–statistics in your command. 


*-*-create-simple-gtf
-----------------

Identical to *-*-create-gtf, but lacks start and stop codon information. This option is significantly faster.


*-*-create-gff3
-----------------

An `Ensembl v3 gff3 <https://useast.ensembl.org/info/website/upload/gff3.html>`_ gff3 will be created that contains mRNA, exon, and intron information. ID, Name, and Parent information will be shown.

