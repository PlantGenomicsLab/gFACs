Output flags
===========================



.. toctree::
   :maxdepth: 2

   Basic_Flags/output.rst
   Fasta_Required_Flags/output.rst
   Basic_Flags/distributions.rst
   Basic_Flags/compatibility.rst