Filter flags
==================

As the title suggests, these flags edit or remove content from the input file. Flag use depends on whether additional resources are provided such as fasta file or EnTAP annotation file. 

.. toctree::
   :maxdepth: 2

   Basic_Flags/index.rst
   EnTAP_Required_Flags/index.rst
   Fasta_Required_Flags/index.rst
