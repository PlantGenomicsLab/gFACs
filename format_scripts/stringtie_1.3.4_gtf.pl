#!usr/bin/perl

# This script is a part of gFACs: https://gitlab.com/PlantGenomicsLab/gFACs
# Copyright (C) 2018 Madison Caballero

open (GTF, $ARGV[0])
	 or die "No input gtf specified!!!";                    # This is the input file specified by gstats.pl
open (OUTPUT, $ARGV[1])
        or die "No out output destination specified!!!";        # This is the output destination specified by gstats.pl

@GTF = <GTF>;
$GTF = join "", @GTF;
@GTF = split "\n", $GTF;							

$temp_loc = $ARGV[1] . "\/" . "\/" . $ARGV[2] . "temp_file.txt";
close GTF;
#####################################################################################
# SORT
open (TEMPORARY_OUTFILE, ">$temp_loc");
foreach $line (@GTF){
#I       StringTie       exon    2402231 2402324 1000    +       .       gene_id "STRG.2"; transcript_id "STRG.2.1"; exon_number "1"; cov "1.212766";
#I       StringTie       exon    2403016 2403438 1000    +       .       gene_id "STRG.2"; transcript_id "STRG.2.1"; exon_number "2"; cov "2.330969";
#I       StringTie       exon    2404645 2404969 1000    +       .       gene_id "STRG.2"; transcript_id "STRG.2.1"; exon_number "3"; cov "5.246154";
#I       StringTie       exon    2406327 2406598 1000    +       .       gene_id "STRG.2"; transcript_id "STRG.2.1"; exon_number "4"; cov "5.014706";
#I       StringTie       exon    2407822 2408076 1000    +       .       gene_id "STRG.2"; transcript_id "STRG.2.1"; exon_number "5"; cov "18.478432";
#I       StringTie       exon    2409282 2409914 1000    +       .       gene_id "STRG.2"; transcript_id "STRG.2.1"; exon_number "6"; cov "14.592417";

	@split = split "\t", $line;
	$split[8] =~ /gene\_id \"STRG\.(\d+)\"\; transcript\_id/;
	$gene_ID = $1;
	if($gene_ID > $max){ $max = $gene_ID; }	
	$store{$gene_ID} = $store{$gene_ID} . "\n" . $line;
}
close TEMPORARY_OUTFILE;

open (TEMPORARY_OUTFILE, ">$temp_loc");
for ($a = 1; $a <= $max; $a++){
	print TEMPORARY_OUTFILE $store{$a};
}
close TEMPORARY_OUTFILE;

####################################################################################
open (GTF, "$temp_loc");
@GTF = <GTF>;
$GTF = join "", @GTF;
@GTF = split "\n", $GTF;


$old_info = 0;
open (TEMPORARY_OUTFILE, ">$temp_loc");
	for ($a = 0; $a < scalar @GTF; $a++){
		$GTF[$a] =~ s/\s+$//;
		@split_line = split "\t", $GTF[$a];

		@split = split "\t", $GTF[$a];
		@info = split ";", $split[8];

		if ($info[0] !~ $old_info){
			print TEMPORARY_OUTFILE "###", "\n",
				$GTF[$a], "\n";
		}
		else{
			print TEMPORARY_OUTFILE $GTF[$a], "\n";
		}
		$old_info = $info[0];
	}
close TEMPORARY_OUTFILE;
close GTF;

####################################################################################
open (GTF, "$temp_loc") or die "Cannot find temp_file.txt at $ARGV[1]!!!";
@GTF = <GTF>;
$GTF = join "", @GTF;
@GTF = split "###", $GTF;
close GTF;

$gene_table = $ARGV[1] . "\/" . "\/" . $ARGV[2] . "gene_table.txt";
open (GENE_TABLE, ">$gene_table") or die "Cannot find gene_table.txt at $ARGV[1]!!!";
print GENE_TABLE "###", "\n";


# This format is peculiar because transcript order needs to be taken into account. 

foreach $gene (@GTF){
	$gene_count = 0;
	$gene =~ s/\s+$//g;
	@lines = split "\n", $gene;

	$count = 0;
	REPEAT: for ($a = 1; $a < 100; $a++){
		$count++;
		$time = 0;
		foreach $line (@lines){
			@split_line = split "\t", $line;
			if ($split_line[2] =~ /transcript/){
				$time++;
				if  ($split_line[3] < $start or $time == 1){$start = $split_line[3];}
				if  ($split_line[4] > $end or $time == 1){$end = $split_line[4];}
				if  (($split_line[4] - $split_line[3] + 1) > $length or $time == 1){
					$length =  $split_line[4] - $split_line[3] + 1;
				}
				$strand = $split_line[6];
			}
		}
		foreach $line (@lines){
			@split_line = split "\t", $line;
			if ($split_line[2] =~ /exon/){
				#gene_id "pita_acchr.3996"; transcript_id "pita_acchr.3996.1";
				# I am getting that .1 number at the end of transcript ID
				
				@info = split ";", $split_line[8];
				$info[1] =~ /^.+\.(\d+)\"$/;
				$ID_name = $info[0];
				$ID = $1;
				if ($ID == $count){
					$exon_length = $split_line[4] - $split_line[3] + 1;
					$ID_name =~ s/gene_id \"//;
					$ID_name =~ s/\"//;

					if ($gene_count == 0 and $count == 1){
						print GENE_TABLE "###", "\n",
						"gene",			 	                # gene
                		                "\t", $length,	                                # length
                                		"\t", $start,	                                # start
		                                "\t", $end,                                     # stop
                		                "\t", $strand,	                                # strand
                                		"\t", "ID=", $ID_name, 		                # ID
		                                 "\t", $split_line[0],
                		                "\n";
						$gene_count = 1;
					} # End of count == 1

					#print $ID, "\t", $count, "\t", $line, "\n";
					print GENE_TABLE "exon",                       # exon
						"\t", $exon_length,				# length
		                                "\t", $split_line[3],                           # start
		                                "\t", $split_line[4],                           # stop
		                                "\t", $split_line[6],                           # strand
		                                "\t", "Parent=", $ID_name, "\." , $count,      # ID
						"\t", $split_line[0],
		                                "\n";

					push @exons, $split_line[3];
					push @exons, $split_line[4];
				} # End of $1 == count

			} # End of if exon
		} # End of foreach line
	} # End of repeat
	if (scalar @exons >= 4){
                @exons_sorted = sort {$a <=> $b} @exons;
                # 10    20      30      40      50      60
                #[0]    [1]-----[2]     [3]-----[4]     [5]
                # Intron = [2] - [1] and [4] - [3]

                for ($a = 2; $a < scalar @exons_sorted; $a = $a +2){
			$end = $exons_sorted[$a] - 1;
                        $start = $exons_sorted[$a-1] + 1;
                        $intron_length = $end - $start + 1;
						
	                        print GENE_TABLE "intron",
	                                "\t", $intron_length,
	                                "\t", $start,
	                                "\t", $end,
	                                "\t", $split_line[6],
					"\t", ".",
					 "\t", $split_line[0],
	                                "\n";		
                } # End of for
	} # End of if @scalar

	undef (@exons);
	undef (@introns);
} # End of gene

close GENE_TABLE;
