#!usr/bin/perl
# This script is a part of gFACs: https://gitlab.com/PlantGenomicsLab/gFACs
# Copyright (C) 2018 Madison Caballero
################################################################################################################################################################
# GENBANK format for annotations

#ARGV[0] --> input gbff
#ARGV[1] --> output location
#ARGV[2] --> prefix

open (GBFF, $ARGV[0]) or die "Cannot oppen $ARGV[0]!!!";

	@gbff = <GBFF>;
	$join = join "", @gbff;
	@gbff = split /\s\s\s+gene\s\s\s+/, $join;

close GBFF;

$temp = $ARGV[1] . "\/" . $ARGV[2] . "temp_file.txt";
$output =  $ARGV[1] . "\/" . $ARGV[2] . "gene_table.txt";

open (TEMP, ">$temp") or die "Cannot create $temp";
	print TEMP "This file is intentionally empty!";
close TEMP;

open (GENE_TABLE, ">$output") or die "Cannot create $output";

################################################################################################################################################################

foreach $gene (@gbff){
	if ($gene =~ /VERSION/){
		$gene =~ s/\n//g;
		$gene =~ /VERSION\s+(.+)DBLINK/;
		$scaffold = $1;
	}
	if ($gene =~ /\s\s\s+CDS\s\s\s+/){
		$gene =~ s/\n//g;
		$gene =~ s/\s+//g;
		@split = split "\/", $gene;
		foreach $segment (@split){
                        if ($segment =~ /(db_xref\=\".+\")/){$db_xref = $1;}
                        if ($segment =~ /(gene\=\".+\")/){$gene_ID = $1;}
			if ($segment =~ /CDS/){									# Set of coordinates
				$CDS_iso++;
				$segment =~ s/^.+\"CDS/CDS/;
				$segment =~ s/CDS/CDS /;
					if ($segment =~ /complement/){$strand = "\-";}
					else{$strand = "\+";}
					if ($segment =~ /\(/){
						$segment =~ /(\(.+\))/;
						$sequences = $1;
					}
					else{
						$segment =~ /(\d+\.\.\d+)/;
						$sequences = $1;
					}		
				@split_segments = split "\,", $sequences;

				foreach $spance (@split_segments){
					if ($spance =~ /\.\./){
						$spance =~ s/\<|\>//g;
						$spance =~ /(\d+\.\.\d+)/;
						$spance_adjusted = $1;
						@split_coordinates = split /\.\./, $spance_adjusted;

						push @coordinates, $split_coordinates[0];
						push @coordinates, $split_coordinates[1];
					}
				}
					
				@sorted_coordinates = sort {$a<=>$b} @coordinates;

				for ($a = 0; $a < scalar @sorted_coordinates; $a = $a + 2){
					$length = $sorted_coordinates[$a+1] - $sorted_coordinates[$a] + 1;
					$exon_line = "exon" . 
						"\t" . $length . 
						"\t" . $sorted_coordinates[$a] .
						"\t" . $sorted_coordinates[$a+1] . 
						"\t" . $strand . 
						"\t" . "Parent\=cds" . $CDS_iso .
						"\t" . $scaffold . 
						"\n";
					push @exon_lines, $exon_line;
					@saved_coordinates = @sorted_coordinates;
				}
				if (scalar @sorted_coordinates > 3){
					for ($a = 2; $a < scalar @sorted_coordinates; $a = $a + 2){
						$intron_start = $sorted_coordinates[$a-1] + 1;
						$intron_end = $sorted_coordinates[$a] - 1;
						$intron_length = $intron_end - $intron_start + 1;
						$intron_line = "intron" .
        		                                "\t" . $intron_length .
                	        	                "\t" . $intron_start .
        	                        	        "\t" . $intron_end .
	                        	                "\t" . $strand .
                        	        	        "\t" . "." .
	                	                        "\t" . $scaffold .
        		                                "\n";
						push @intron_lines, $intron_line;
					}
				}
				undef (@coordinates);
			} #If Segment is CDS
		} #Foreach segment

		$length = $saved_coordinates[scalar @saved_coordinates -1] - $saved_coordinates[0] + 1;

		print GENE_TABLE "###", "\n",
                "gene",
                "\t", $length,
                "\t", $saved_coordinates[0],
                "\t", $saved_coordinates[scalar @saved_coordinates -1],
                "\t", $strand,
                "\t", $gene_ID . $db_xref,
                "\t", $scaffold,
                "\n";

		foreach $exon (@exon_lines){
			print GENE_TABLE $exon;
		}
		foreach $intron (@intron_lines){
                        print GENE_TABLE $intron;
                }

	# All deleted as we go onto the next gene
	undef (@exon_lines);
	undef (@intron_lines);
	undef ($db_xref);
	undef ($gene_ID);
	undef ($CDS_iso);
	}#Only genes I care about \s\s+CDS
}# Foreach gene

print GENE_TABLE "\n###\n";
close GENE_TABLE;
##################################################################################################################################################################
# This was BY FAR the most annoyting format to configure 
